#!/usr/bin/python3

from subprocess import call
import os
import errno
from shutil import copy2

real_home = os.path.expanduser("~")  # '/home/will'
home = '/home/will/.falsehome/'
local_working_dir = '/home/will/.tmp/'

remote_host = 'ahchto'


def ensure_dir(dir):
    if not os.path.exists(dir):
        try:
            os.makedirs(dir)
        # this exception is form stackoverflow
        # do more research
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise


def get_files():
    return dict([('kdeglobals', "/.kde/share/config/"),
                 ('konsolerc', "/.config/"),
                 ('kwinrc', "/.config/"),
                 ('kwinrulesrc', "/.config/")])
    # return [home + "/.kde/share/config/kdeglobals", home + "/.config/konsolerc", home + "/.config/kwinrc", home+"/config/kwinrulesrc"]


def create_globbed_string(list):
    return '{' + ','.join(list) + '}'


def push_files(files):
    file_list = []
    for file in files:
        file_list.append(real_home + files[file] + file)

    call(['rsync'] + file_list+[remote_host + ':~/.sync'])


def pull_files(files):
    ensure_dir(home)
    ensure_dir(local_working_dir)
    call(['rsync', remote_host + ':~/.sync/*', local_working_dir])
    print('pulled files to ' + local_working_dir)

    files_and_destinations = get_files()
    for file in os.listdir(local_working_dir):
        if file in files_and_destinations:
            print(file + ' belongs at ' + files_and_destinations[file])
            ensure_dir(home + files_and_destinations[file])

            copy2(local_working_dir + file, home +
                  files_and_destinations[file] + file)
            # print(os.stat(home + files_and_destinations[file] + file))
            if not os.path.exists(home + files_and_destinations[file] + file):
                print('FILE DID NOT COPY')
        else:
            print('file with no destination: ' + file)


push_files(get_files())
pull_files(get_files())
